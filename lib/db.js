//Connection du projet à la base de données

var mysql = require('mysql');
var connection = mysql.createConnection({
	host:'localhost',
	user:'root',
	password:'',
	database:'projet-mairie'
});
connection.connect(function(error){
	if(!!error) {
		console.log(error);
	} else {
		console.log('Connected Projet-Mairie..!');
	}
});

module.exports = connection;